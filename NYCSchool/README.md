# NY School List App

This is a simple app that shows
List of schools in NYC
SAT Scores and additional details about school

## Structure

App is in SwiftUI
App follows simple MVVM patten
ServiceManager is used to make API calls from view models using NetworkInterface
Endpoint is a protocol to create request resource for each API

## Unit Tests

Unit tests are writtent to cover major business logic in view models.

![Sample Video](AppScreens/demo.mp4)
