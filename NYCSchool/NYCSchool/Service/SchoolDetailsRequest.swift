//
//  SchoolDetailsRequest.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 24/07/23.
//

import Foundation

struct SchoolDetailsRequest: Endpoint {
    
    let id: String
    
    var path: String {
        return Constants.APIPath.satScores + id
    }
    
    var method: RequestMethod {
        return .get
    }
}
