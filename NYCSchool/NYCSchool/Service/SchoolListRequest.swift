//
//  SchoolListRequest.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 20/07/23.
//

import Foundation

struct SchoolListRequest: Endpoint {
    
    var path: String { Constants.APIPath.schoolList }
    
    var method: RequestMethod { .get }
}
