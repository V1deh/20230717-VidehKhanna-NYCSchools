//
//  ServiceInterface.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 17/07/23.
//

import Foundation
import SystemConfiguration

/// Enum to contain all type of used Http Request method
enum RequestMethod: String {

    /// Get Request
    case get = "GET"

    /// Post Request
    case post = "POST"
}


// Define an enumeration for network environment
enum NetworkEnvironment {
    case production
    case staging
    case development
    
    var baseURL: String {
        switch self {
        case .production:
            return "https://data.cityofnewyork.us"
        default:
            return "https://data.cityofnewyork.us"
        }
    }
}

/// API Errors
enum AppError: Swift.Error {
    case invalidURL
    case httpCode(Int)
    case unexpectedResponse
    case emptyData
    case noInternet
    case decodingError
}

/// Endpoint Interface to confirm mandatory URL related properties
protocol Endpoint {
    
    var path: String { get }
    var method: RequestMethod { get }
    var parameters: [String: Any] { get }
    var headers: [String: String] { get }
}

/// Extension for endpoint for default implementation
extension Endpoint {
    var headers: [String: String] {
        [Constants.Header.contentType: Constants.Header.applicationJson,
         Constants.Header.appToken: Constants.Keys.apiKey]
    }
    
    var parameters: [String: Any] {
        [:]
    }
}

/// Network Protocol to create an async URL request
protocol NetworkInterface {
    
    /// This method creates a url request, parse response, identifies errors and success data models and implement Retry if needed
    /// - Parameters:
    ///   - endpoint: Endpoint interface
    ///   - responseModel: Response Model Interface
    /// - Returns: Result type of Response Model and Error
    func sendRequest<T: Decodable>(request: Endpoint, responseModel: T.Type) async throws -> Result<T, Error>
}

/// Extension to implement send request method
extension NetworkInterface {
    
    /// Environment
    private var environment: NetworkEnvironment {
#if DEBUG
        return .development
#else
        return .production
#endif
    }

    /// Send request default implementation
    func sendRequest<T: Decodable>(request: Endpoint,
                                   responseModel: T.Type) async throws -> Result<T, Error> {

        guard isNetworkReachable() else {
            Logger.error(items: AppError.noInternet.localizedDescription)
            return .failure(AppError.noInternet)
        }
        
        guard let url = URL(string: environment.baseURL + request.path) else {
            Logger.error(items: AppError.invalidURL.localizedDescription)
            return .failure(AppError.invalidURL)
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method.rawValue
        urlRequest.allHTTPHeaderFields = request.headers
        
        if !request.parameters.isEmpty {
            urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: request.parameters, options: [])
        }
        
        Logger.debug(items: "API: \(url)")
        
        return try await withCheckedThrowingContinuation { checkedContinuation in
            let task = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
                if let error = error {
                    Logger.error(items: error.localizedDescription)
                    checkedContinuation.resume(with: .failure(error))
                    return
                    
                } else {
                    guard let data = data, let code = (response as? HTTPURLResponse)?.statusCode else {
                        Logger.error(items: AppError.unexpectedResponse.localizedDescription)
                        checkedContinuation.resume(with: .failure(AppError.unexpectedResponse))
                        return
                    }
                    switch code {
                    case -200 ..< 300:
                        do {
                            let decoder = JSONDecoder()
                            let result = try decoder.decode(T.self, from: data)
                            checkedContinuation.resume(with: .success(.success(result)))
                            return
                        } catch {
                            Logger.error(items: (error.localizedDescription))
                            checkedContinuation.resume(with: .failure(error))
                            return
                        }
                    default:
                        Logger.error(items: AppError.httpCode(code))
                        checkedContinuation.resume(with: .failure(AppError.httpCode(code)))
                        return
                    }
                    
                }
            }
            
            task.resume()
        }
    }
    
    /// Define a function to check network reachability
    func isNetworkReachable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }), var flags = SCNetworkReachabilityFlags(rawValue: 0) as SCNetworkReachabilityFlags? else {
            return false
        }
        
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
}

class ServiceManager: NetworkInterface {
    
    static let shared = ServiceManager()
    
    private init() { }
}
