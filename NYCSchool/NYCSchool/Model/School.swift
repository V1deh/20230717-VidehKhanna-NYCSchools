//
//  School.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 18/07/23.
//

import Foundation

struct School: Codable, Identifiable, Hashable {
    var id: String { dbn }
    
    let dbn: String
    let schoolName: String
    let overviewParagraph: String
    let academicOpportunities1: String?
    let academicOpportunities2: String?
    let neighborhood: String?
    let location: String?
    let phoneNumber: String?
    let faxNumber: String?
    let schoolEmail: String?
    let website: String?
    let subway: String?
    let bus: String?
    let finalGrades: String?
    let totalStudents: String?
    let extracurricularActivities: String?
    let schoolSports: String?
   
    
    enum CodingKeys: String, CodingKey {
        case dbn, neighborhood, location, website, subway, bus
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case academicOpportunities1 = "academicopportunities1"
        case academicOpportunities2 = "academicopportunities2"
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case finalGrades = "finalgrades"
        case totalStudents = "total_students"
        case extracurricularActivities = "extracurricular_activities"
        case schoolSports = "school_sports"
    }
}
