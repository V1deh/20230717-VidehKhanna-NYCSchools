//
//  NYCSchoolApp.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 17/07/23.
//

import SwiftUI

@main
struct NYCSchoolApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolListView()
        }
    }
}
