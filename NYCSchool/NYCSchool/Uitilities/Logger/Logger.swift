//
//  Logger.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 17/07/23.
//

import Foundation

/**
 Class for adding Debug logs
    - Contains functions for logging
    1. Debug
    2. Error

 */
class Logger {

    /**
    Function to print Debug log
    */
    class func debug(items: Any..., fileName: String = #file, functionName: String = #function, lineNumber: Int = #line) {
        #if DEBUG
        print("NS_DEBUG: \(URL(fileURLWithPath: fileName).lastPathComponent) : \(functionName) : Line# \(lineNumber) - \(items)")
        #endif
    }

    /**
    Function to print Error log
    */
    class func error(items: Any..., fileName: String = #file, functionName: String = #function, lineNumber: Int = #line) {
        #if DEBUG
        print("NS_ERROR: \(URL(fileURLWithPath: fileName).lastPathComponent) : \(functionName) : Line# \(lineNumber) - \(items)")
        #endif
    }
}

