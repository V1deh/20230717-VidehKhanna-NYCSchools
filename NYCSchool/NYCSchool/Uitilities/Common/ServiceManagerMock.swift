//
//  ServiceManagerMock.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 24/07/23.
//

import Foundation

/// Dummy ServiceManager implementation to mock API response
class ServiceManagerMock: NetworkInterface {
    var shouldReturnError = false
    var mockFileName = ""
    
    func sendRequest<T: Decodable>(request: Endpoint,
                                   responseModel: T.Type) async throws -> Result<T, Error> {
        if shouldReturnError {
            return .failure(AppError.unexpectedResponse)
        }
        
        if let data = MockData.shared.loadJSONData(fileName: mockFileName) {
            do {
                let decodedData = try JSONDecoder().decode(T.self, from: data)
                   return .success(decodedData)
            } catch {
                return .failure(AppError.decodingError)
            }
        } else {
            return .failure(AppError.unexpectedResponse)
        }
    }
}

/// Class to load mock data from json files
class MockData {
    
    static let shared = MockData()
    
    private init() {}
    
    func loadJSONData(fileName: String) -> Data? {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                return data
            } catch {
                Logger.error(items: ("Error reading mock data from file: \(fileName)"))
                return nil
            }
        } else {
            Logger.error(items: ("File not found: \(fileName)"))
            return nil
        }
    }
}

