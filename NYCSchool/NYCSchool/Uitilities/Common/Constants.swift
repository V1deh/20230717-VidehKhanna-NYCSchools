//
//  Constants.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 20/07/23.
//

import Foundation

struct Constants {
    struct Header {
        static let contentType = "Content-Type"
        static let applicationJson = "application/json"
        static let appToken = "X-App-Token"
    }
    
    struct FileName {
        static let schoolList = "MockSchoolList"
        static let satScores = "MockSATScore"
    }
    
    struct ExternalURL {
        static let web = "SchoolList"
        static let phone = "SATScores"
    }
    
    struct APIPath {
        static let schoolList = "/resource/s3k6-pzi2.json?$limit=20"
        static let satScores = "/resource/f9bf-2cp4.json?dbn="
    }
    
    struct Keys {
        static let apiKey = "51BlyaqFG39VV9VMBJl0jt90h"
    }
}

struct ImageName {
    static let location = "location.fill"
    static let mail = "mail"
    static let phone = "phone"
    static let squareroot = "x.squareroot"
    static let book = "book"
    static let pencil = "pencil"
    static let person = "person.3"
}

struct LayoutConstants {
    static let marginSmall: CGFloat = 8
    static let marginDefaultSmall: CGFloat = 12
    static let marginDefault: CGFloat = 16
    static let marginMedium: CGFloat = 24
    static let marginLarge: CGFloat = 32
}
