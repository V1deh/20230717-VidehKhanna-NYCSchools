//
//  SchoolListView.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 17/07/23.
//

import SwiftUI

struct SchoolListView: View {
    
    private struct Constants {
        static let errorText = "There was an error loading, Please try again later."
        static let navigationTitle = "NYC School List"
    }
    
    @ObservedObject var viewModel = SchoolListViewModel()
    
    var body: some View {
        NavigationStack {
            VStack {
                if viewModel.isLoading {
                    Spacer()
                    ProgressView()
                        .padding(.top)
                    Spacer()
                } else if viewModel.showError {
                    Text(Constants.errorText)
                        .foregroundColor(.red)
                        .padding()
                } else {
                    ScrollView(showsIndicators: false) {
                        ForEach(viewModel.schools, id: \.self) { school in
                            NavigationLink(value: school) {
                                SchoolCardView(cardModel: CardModel(schoolName: school.schoolName,
                                                                    location: school.location,
                                                                    email: school.schoolEmail,
                                                                    phoneNumber: school.phoneNumber))
                                .padding(.top)
                                .padding([.leading, .trailing])
                            }
                        }
                    }
                }
            }.navigationTitle(Text(Constants.navigationTitle))
             .navigationBarTitleDisplayMode(.inline)
             .navigationDestination(for: School.self) { school in
                 SchoolDetailsView(viewModel: SchoolDetailsViewModel(school: school))
             }
        }.task {
            await viewModel.fetchSchoolList()
        }
    }
}

struct SchoolListView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListView()
    }
}
