//
//  SchoolCardView.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 20/07/23.
//

import SwiftUI

// MARK: - CardModel

/// Model to populate card view
struct CardModel {
    let schoolName: String?
    let location: String?
    let email: String?
    let phoneNumber: String?
}

struct SchoolCardView: View {
    
    /// Constants
    private struct Constants {
        static let pinFrame: CGFloat = 18
        static let mailIconHeight: CGFloat = 18
        static let phoneIconFrame: CGFloat = 24
        static let cardWidth: CGFloat = UIScreen.main.bounds.width - 64
        static let radius: CGFloat = 5
        static let shadowRadius: CGFloat = 5
        static let mailTo = "mailto:"
        static let telephone = "tel://+1"
    }
    
    /// Card model
    let cardModel: CardModel
    
    var body: some View {
        VStack(alignment: .leading) {
            if let schoolName = cardModel.schoolName {
                Text(schoolName)
                    .foregroundColor(.black)
                    .font(.title2)
                    .multilineTextAlignment(.leading)
            }
            
            if let location = cardModel.location {
                HStack {
                    Image(systemName: ImageName.location)
                        .resizable()
                        .foregroundColor(.black)
                        .frame(width: Constants.pinFrame, height: Constants.pinFrame)
                    
                    Text(location)
                        .font(.body)
                        .foregroundColor(.black.opacity(0.8))
                        .multilineTextAlignment(.leading)
                        .lineSpacing(2)
                }
            }
            if let email = cardModel.email {
                HStack {
                    Image(systemName: ImageName.mail)
                        .resizable()
                        .foregroundColor(.black)
                        .frame(width: LayoutConstants.marginMedium, height: Constants.mailIconHeight)
                    
                    Text(email)
                        .foregroundColor(.blue)
                        .font(.body)
                        .underline(true, color: .blue)
                }.padding(.top, LayoutConstants.marginSmall)
                 .onTapGesture {
                     guard let mailUrl = URL(string: Constants.mailTo + email),
                           UIApplication.shared.canOpenURL(mailUrl) else { return }
                     UIApplication.shared.open(mailUrl)
                }
            }
            
            if let phoneNumber = cardModel.phoneNumber {
                HStack {
                    Image(systemName: ImageName.phone)
                        .resizable()
                        .foregroundColor(.black)
                        .frame(width: Constants.mailIconHeight, height: Constants.mailIconHeight)
                    
                    Text(phoneNumber)
                        .foregroundColor(.accentColor)
                        .font(.body)
                        .underline(true, color: .accentColor)
                }.padding(.top, LayoutConstants.marginSmall)
                .onTapGesture {
                    guard let phoneUrl = URL(string: Constants.telephone + phoneNumber),
                            UIApplication.shared.canOpenURL(phoneUrl) else { return }
                    UIApplication.shared.open(phoneUrl)
                }
            }
        }.frame(width: Constants.cardWidth)
         .padding([.top, .bottom])
         .padding(.leading, LayoutConstants.marginSmall)
         .padding(.trailing, LayoutConstants.marginDefaultSmall)

        .background(Rectangle()
            .fill(Color.white)
            .cornerRadius(Constants.radius)
            .shadow(radius: Constants.shadowRadius))
    }
}

struct SchoolCardView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolCardView(cardModel: CardModel(schoolName: "Clinton School Writers & Artists, M.S. 260", location: "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)", email: "admissions@theclintonschool.net", phoneNumber: "212-524-4360"))
    }
}
