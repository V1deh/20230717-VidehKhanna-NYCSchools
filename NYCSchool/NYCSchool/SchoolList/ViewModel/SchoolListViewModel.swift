//
//  SchoolListViewModel.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 20/07/23.
//

import Foundation

class SchoolListViewModel: ObservableObject {
    
    @Published var schools: [School] = []
    @Published var isLoading = false
    @Published var showError = false
    
    private let serviceRequest: NetworkInterface
    
    // Make service manager mockable and testable
    init(serviceRequest: NetworkInterface = ServiceManager.shared) {
        self.serviceRequest = serviceRequest
    }
    
}

// MARK: - Extension for API
extension SchoolListViewModel {
    
    /// Function to fetch school list
    func fetchSchoolList() async {
        Task(priority: .userInitiated) {
            DispatchQueue.main.async { [weak self] in
                self?.isLoading = true
            }
            do {
                let result = try await serviceRequest.sendRequest(request: SchoolListRequest(), responseModel: [School].self)
            
                switch result {
                case .success(let response):
                    DispatchQueue.main.async { [weak self] in
                        self?.schools = response
                        self?.showError = false
                        self?.isLoading = false
                    }
                case .failure(_):
                    DispatchQueue.main.async { [weak self] in
                        self?.showError = true
                        self?.isLoading = false
                    }
                }
            } catch {
                DispatchQueue.main.async { [weak self] in
                    self?.showError = true
                    self?.isLoading = false
                }
            }
        }
    }
}
