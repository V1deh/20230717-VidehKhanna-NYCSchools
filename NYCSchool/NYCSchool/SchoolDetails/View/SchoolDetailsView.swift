//
//  SchoolDetailsView.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 24/07/23.
//

import SwiftUI

struct SchoolDetailsView: View {
    
    private struct Constants {
        static let errorMessage = "SAT data could not be found"
        static let numberOfStudents = "Total number of student's:"
        static let mathAvgScore = "Math Avg Score"
        static let readingAvgScore = "Reading Avg Score"
        static let writingAvgScore = "Writing Avg Score"
        static let numberOfTestTaker = "No of Test Taker's"
        static let overviewLineSpacing: CGFloat = 3
        static let imageSize = CGFloat(13)
    }
    
    @StateObject var viewModel: SchoolDetailsViewModel
    
    var body: some View {
        ScrollView {
            VStack {
                if viewModel.isLoading {
                    Spacer()
                    ProgressView()
                        .padding(.top)
                    Spacer()
                } else if viewModel.showError {
                    Text(Constants.errorMessage)
                        .foregroundColor(.red)
                        .padding()
                } else {
                    headerView()
                        .padding(.top)
                    
                    Rectangle()
                        .frame(width: UIScreen.main.bounds.width - LayoutConstants.marginLarge, height: 1)
                        .padding(.top)
                    
                    if let totalStudents = viewModel.school.totalStudents {
                        Text(Constants.numberOfStudents + " " + totalStudents)
                            .foregroundColor(.black.opacity(0.7))
                            .font(.headline)
                            .lineSpacing(Constants.overviewLineSpacing)
                            .padding([.top, .trailing])
                            .padding(.leading, LayoutConstants.marginSmall)
                            .fixedSize(horizontal: false, vertical: true)
                    }
                    
                    Rectangle()
                        .frame(width: UIScreen.main.bounds.width - LayoutConstants.marginLarge, height: 1)
                        .padding(.top)
                    
                    averageSatScore()
                        .padding(.top, LayoutConstants.marginSmall)
                    
                    Rectangle()
                        .frame(width: UIScreen.main.bounds.width - LayoutConstants.marginLarge, height: 1)
                }
            }
        }.task {
            await viewModel.fetchScoreDetails()
        }
    }
    
    /// Header view
    private func headerView() -> some View {
        VStack(alignment: .leading) {
            Text(viewModel.school.schoolName)
                .foregroundColor(.black)
                .font(.headline)
                .padding(.trailing)
                .padding(.leading, LayoutConstants.marginSmall)
                .fixedSize(horizontal: false, vertical: true)
            
            Text(viewModel.school.overviewParagraph)
                .foregroundColor(.black.opacity(0.7))
                .font(.body)
                .lineSpacing(Constants.overviewLineSpacing)
                .padding([.top, .trailing])
                .padding(.leading, LayoutConstants.marginSmall)
                .fixedSize(horizontal: false, vertical: true)
        }
    }
    
    /// Average SAT score
    private func averageSatScore() -> some View {
        Group {
            if let satScores = viewModel.scoreDetails {
                HStack {
                   
                    imageWithText(headline: Constants.mathAvgScore, imageName: ImageName.squareroot, text: satScores.satMathAvgScore)
                    
                    Rectangle().frame(width: 1)
                    
                    imageWithText(headline: Constants.readingAvgScore, imageName: ImageName.book, text: satScores.satCriticalReadingAvgScore)
                    
                    Rectangle().frame(width: 1)
                    
                    imageWithText(headline: Constants.writingAvgScore, imageName: ImageName.pencil, text: satScores.satWritingAvgScore)
                    
                    Rectangle().frame(width: 1)
                    
                    imageWithText(headline: Constants.numberOfTestTaker, imageName: ImageName.person, text: satScores.numOfSatTestTakers)
                }.frame(width: UIScreen.main.bounds.width - LayoutConstants.marginLarge)
            }
        }
    }
    
    private func imageWithText(headline: String, imageName: String, text: String) -> some View {
        VStack(alignment: .center) {
            Text(headline)
                .font(.body)
            
            HStack {
                Image(systemName: imageName)
                    .resizable()
                    .frame(width: LayoutConstants.marginDefault, height: LayoutConstants.marginDefault)
                
                Text(text)
                    .font(.callout)
                    .foregroundColor(.black)
            }
        }
    }
}

struct SchoolDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailsView(viewModel: SchoolDetailsViewModel(school: School(dbn: "21K728", schoolName: "Clinton School Writers & Artists, M.S. 260", overviewParagraph: "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.", academicOpportunities1: nil, academicOpportunities2: nil, neighborhood: nil, location: "dasd", phoneNumber: "9879", faxNumber: nil, schoolEmail: "a@a.com", website: "sda.com", subway: nil, bus: nil, finalGrades: nil, totalStudents: "500", extracurricularActivities: nil, schoolSports: nil)))
    }
}
