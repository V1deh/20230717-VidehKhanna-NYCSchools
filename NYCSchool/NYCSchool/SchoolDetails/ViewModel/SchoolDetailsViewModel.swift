//
//  SchoolDetailsViewModel.swift
//  NYCSchool
//
//  Created by Khanna, Videh Rakesh Rakesh on 24/07/23.
//

import Foundation

class SchoolDetailsViewModel: ObservableObject {
    
    @Published var school: School
    
    @Published var scoreDetails: ScoreDetails?
    
    @Published var isLoading = false
    
    @Published var showError = false
    
    private let serviceRequest: NetworkInterface
    
    // Make service manager mockable and testable
    init(school: School, serviceRequest: NetworkInterface = ServiceManager.shared) {
        self.serviceRequest = serviceRequest
        self.school = school
    }
    
    func fetchScoreDetails() async {
        Task(priority: .userInitiated) {
            DispatchQueue.main.async { [weak self] in
                self?.isLoading = true
            }
            
            do {
                let result = try await serviceRequest.sendRequest(request: SchoolDetailsRequest(id: school.dbn), responseModel: [ScoreDetails].self)
                
                DispatchQueue.main.async { [weak self] in
                    self?.isLoading = false
                }
                switch result {
                case .success(let response):
                    DispatchQueue.main.async { [weak self] in
                        if let scoreDetails = response.first {
                            self?.scoreDetails = scoreDetails
                            self?.showError = false
                        } else {
                            self?.showError = true
                        }
                    }
                case .failure(_):
                    DispatchQueue.main.async { [weak self] in
                        self?.showError = true
                    }
                }
            }
        }
    }
}
