//
//  SchoolDetailsViewModelTests.swift
//  NYCSchoolTests
//
//  Created by Khanna, Videh Rakesh Rakesh on 24/07/23.
//

import XCTest
import Foundation

final class SchoolDetailsViewTests: XCTestCase {
    
    var viewModel: SchoolDetailsViewModel!
    var serviceManagerMock: ServiceManagerMock!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
        
        serviceManagerMock = ServiceManagerMock()
        serviceManagerMock.mockFileName = Constants.FileName.satScores
        
        // Load mock data from json
        if let data = MockData.shared.loadJSONData(fileName: Constants.FileName.schoolList) {
            do {
                let mockSchools = try JSONDecoder().decode([School].self, from: data)
                if let selectedSchool =  mockSchools.first {
                    viewModel = SchoolDetailsViewModel(school: selectedSchool, serviceRequest: serviceManagerMock)
                } else {
                    throw AppError.decodingError
                }
            } catch {
                throw AppError.emptyData
            }
        } else {
            throw AppError.unexpectedResponse
        }
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        try super.tearDownWithError()
        
        self.serviceManagerMock = nil
        self.viewModel = nil
        
    }
    
    func testFetchSATScorestSuccess() async {
        let expectation = XCTestExpectation(description: "Fetch SAT score successfully")
        
        // Test SATScores function from mock file
        await viewModel.fetchScoreDetails()
        
        // Check Results and Add Assertions
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // SAT scores should be fetched
            XCTAssertNotNil(self.viewModel.scoreDetails)
            
            // There should not be any errors
            XCTAssertFalse(self.viewModel.showError)
            expectation.fulfill()
        }
        
        await fulfillment(of: [expectation])
    }
    
    func testFetchSchoolListFailure() async {
        let expectation = XCTestExpectation(description: "Fetch SAT score failure")
        serviceManagerMock.shouldReturnError = true
        
        // Test fetchScoreDetails function from mock file
        await viewModel.fetchScoreDetails()
        
        // Check Results and Add Assertions
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            // SAT Scores should be nil
            XCTAssertNil(self.viewModel.scoreDetails)
            
            // ViewModel should be have error
            XCTAssertTrue(self.viewModel.showError)
            expectation.fulfill()
        }
        
        await fulfillment(of: [expectation])
    }
}
