//
//  SchoolListViewModelTests.swift
//  NYCSchoolTests
//
//  Created by Khanna, Videh Rakesh Rakesh on 24/07/23.
//

import XCTest

final class SchoolListViewModelTests: XCTestCase {
    
    var viewModel: SchoolListViewModel!
    var serviceManagerMock: ServiceManagerMock!
    
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
        
        serviceManagerMock = ServiceManagerMock()
        serviceManagerMock.mockFileName = Constants.FileName.schoolList
        viewModel = SchoolListViewModel(serviceRequest: serviceManagerMock)
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        try super.tearDownWithError()
        
        self.serviceManagerMock = nil
        self.viewModel = nil
        
    }
    
    func testFetchSchoolListSuccess() async {
        // Testcase -
        let expectation = XCTestExpectation(description: "Fetch schools successfully")
        serviceManagerMock.shouldReturnError = false
        // Test fetchSchoolList function from mock file
        await viewModel.fetchSchoolList()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // Exactly one article should be loaded
            XCTAssertEqual(self.viewModel.schools.count, 1)
            
            // There should not be any errors
            XCTAssertFalse(self.viewModel.showError)
            expectation.fulfill()
        }
        
        await fulfillment(of: [expectation])
    }
    
    func testFetchSchoolListFailure() async {
        // Testcase -
        let expectation = XCTestExpectation(description: "Fetch schools failure")
        serviceManagerMock.shouldReturnError = true
        
        // Test fetchSchoolList function from mock file
        await viewModel.fetchSchoolList()
        
        // Check Results and Add Assertions
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            // No articles should be fetched if request fails
            XCTAssertEqual(self.viewModel.schools.count, 0)
            
            // ViewModel should be have error
            XCTAssertTrue(self.viewModel.showError)
            expectation.fulfill()
        }
        
        await fulfillment(of: [expectation])
    }
}
